package com.antonioandroid.app;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class StepCounterActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private TextView count;
    private Vibrator vibrator;
    private Button startStopButton;
    private boolean isCounting;
    private Sensor stepCounterSensor;
    private static final float STEP_LENGTH = 0.762f; // longueur de pas en mètres
    private TextView distanceTextView;
    private Button resetButton;
    private int initialStepCount = -1; // Cette variable garde la valeur initiale au moment de la réinitialisation
    private int stepsSinceReset = 0;
    private TextView statusTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        count = findViewById(R.id.stepCountTextView);
        count = (TextView) findViewById(R.id.stepCountTextView);
        statusTextView = findViewById(R.id.statusTextView); // assurez-vous que cet ID correspond à celui de votre layout XML

        distanceTextView = (TextView) findViewById(R.id.distanceTextView);  // Initialisation de la variable



        startStopButton = findViewById(R.id.startStopButton); // assurez-vous que cet ID correspond à celui de votre layout XML

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepCounterSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        resetButton = (Button) findViewById(R.id.resetButton); // Initialisation du bouton de réinitialisation
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResetButtonClicked();
            }
        });

        if (stepCounterSensor == null) {
            count.setText("Compteur de pas non disponible!");
        }

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Démarrer le comptage des pas
                if (vibrator.hasVibrator()) {
                    vibrator.vibrate(500); // 500 ms pour l'exemple
                }
                if (!isCounting) {

                    isCounting = true;
                    startStopButton.setText("Stop");
                    if (stepCounterSensor != null) {
                        sensorManager.registerListener(StepCounterActivity.this, stepCounterSensor, SensorManager.SENSOR_DELAY_UI);
                    }
                } else {
                    // Arrêter le comptage des pas
                    isCounting = false;
                    startStopButton.setText("Start");
                    sensorManager.unregisterListener(StepCounterActivity.this);
                }
            }
        });
    }

    private void onResetButtonClicked() {
        // Réinitialiser le compteur de pas initial pour capturer la nouvelle valeur de base lors de la prochaine lecture du capteur
        initialStepCount = -1;
        stepsSinceReset = 0; // Réinitialiser les pas comptés depuis la dernière réinitialisation

        // Réinitialiser les valeurs affichées
        count.setText("Steps: 0");
        distanceTextView.setText("Distance: 0 m");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (stepCounterSensor != null && isCounting) {
            sensorManager.registerListener(this, stepCounterSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isCounting) {
            sensorManager.unregisterListener(this);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (isCounting) {
            if (initialStepCount == -1) { // Si c'est la première lecture ou après une réinitialisation
                initialStepCount = (int) event.values[0];
            }

            // Calculer les pas depuis la réinitialisation
            stepsSinceReset = (int) event.values[0] - initialStepCount;

            // Mettre à jour l'affichage des pas
            count.setText(String.valueOf(stepsSinceReset));

            // Calculer la distance depuis la réinitialisation
            float distanceInMeters = stepsSinceReset * STEP_LENGTH;
            distanceTextView.setText(String.format("%.2f mètres", distanceInMeters));
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
